# CPP Skeleton

A made-up project to help develop an understanding of modern cmake. There will 
be missunderstandings on my part. I will edge those out as I go along and get
a better understanding of cmake.

# Build Instructions

From the source directory

### CppSkeleton

CppSkeleton currently depends on Catch2, for this build purpose you would have
to build and install Catch2 to a location from source before proceeding. To
build without tests use the -DBUILD_TESTS=OFF flag.

```bash
mkdir build && cd build
# To build without Catch2 
# cmake -DCMAKE_INSTALL_PREFIX=$(pwd)/_install -DBUILD_TESTS=OFF -DCMAKE_BUILD_TYPE=Release ..
cmake -DCMAKE_INSTALL_PREFIX=$(pwd)/_install -DCatch2_DIR=/path/to/catch2/lib/cmake -DCMAKE_BUILD_TYPE=Release ..
cmake --build . --target install
```

### Example Exec

```bash
cd _example_exec
mkdir build && cd build
# example_exec CMakeLists.txt does not have a install target, you can move the
# generated exeutable example_exec to anywhere as the RUNPATH is set.
cmake -DCppSkeleton_DIR=/path/to/cppskeleton/lib64/cmake .. && cmake --build .
./example_exec
Hello from example exec
```

## Credits & Resources

* [CLIUtils](https://gitlab.com/CLIUtils/modern-cmake)
* [Effective Modern CMake](https://gist.github.com/mbinna/c61dbb39bca0e4fb7d1f73b0d66a4fd1)
* [It's Time To Do CMake Right](https://pabloariasal.github.io/2018/02/19/its-time-to-do-cmake-right/)
* [Public, Private & Interface](https://cmake.org/pipermail/cmake/2016-May/063400.html)
* [Awesome CMake](https://github.com/onqtam/awesome-cmake)

## Useful Links to CMake Documentation

* [CMake Build Systems](https://cmake.org/cmake/help/v3.12/manual/cmake-buildsystem.7.html#manual:cmake-buildsystem(7))
* [CMake Generator Expressions](https://cmake.org/cmake/help/v3.11/manual/cmake-generator-expressions.7.html#manual:cmake-generator-expressions(7))
* [Specifying C++ Standard](https://stackoverflow.com/a/20165220)
* [Specify C++ Compiler Extension](https://cmake.org/cmake/help/v3.1/prop_tgt/CXX_EXTENSIONS.html)

## Debugging

* [CMake Print](https://cmake.org/cmake/help/latest/module/CMakePrintHelpers.html)
* [System Information](https://cmake.org/cmake/help/latest/module/CMakePrintSystemInformation.html)
* [Variable Watch](https://cmake.org/cmake/help/latest/command/variable_watch.html)
* [Variables](https://cmake.org/cmake/help/latest/prop_dir/VARIABLES.html)

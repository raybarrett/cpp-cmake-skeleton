#include <iostream>
#include <cpp-skeleton/utils.h>

int main() {
  std::cout << utils::say_hello("user") << std::endl;
}

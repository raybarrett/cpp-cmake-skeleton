#pragma once

#include <string>

namespace utils
{
  /* 
   * Function for saying hello
   *
   * @param name is the name of the input user
   */
  std::string say_hello(const std::string& name);
  
} // utils

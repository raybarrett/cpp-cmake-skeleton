
#include <catch2/catch.hpp>
#include <cpp-skeleton/utils.h>

TEST_CASE("Saying Hello", "[say_hello]") {
  REQUIRE(utils::say_hello("user") == "Hello user");
}



// With catch in a several file test build it's better to have one file handle
// the main build and link against it
#define CATCH_CONFIG_MAIN

#include <catch2/catch.hpp>

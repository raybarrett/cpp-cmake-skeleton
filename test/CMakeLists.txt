
# This is how you might do something if you've built Catch2 from source, as that
# will provide the install location with additonal package files for cmake to
# work. Another approach is to have it as a git submodule of the package, then
# you would be able to generate the Catch2 target yourself and might be less
# hassle. It all depends on your environment though

# Catch2_DIR depending on your environment should either be given as a
# commandline argument or can be set here if you are in a dynamic environment
# generated with REZ. By specifying CACHE STRING we make the variable
# overrideable from the command line.
set(Catch2_DIR "/path/to/catch2/cmake" CACHE STRING "Catch2 Config Location")
find_package(Catch2 CONFIG REQUIRED)

# Build the catch object interface which can be reused for every test
add_library(Catch2Main OBJECT src/main.cpp)
target_link_libraries(Catch2Main PRIVATE Catch2::Catch2)
add_library(Catch2::Main ALIAS Catch2Main)

# Create test executables
add_executable(test_utils $<TARGET_OBJECTS:Catch2::Main> src/test_utils.cpp)
target_link_libraries(test_utils PRIVATE ${PROJECT_NAME} Catch2::Catch2)

# Enable testing will add a test target that we can run from the build/test
# directory
enable_testing()
# The add_test adds a command to be executed during a test run
add_test(NAME test_utils COMMAND test_utils)
